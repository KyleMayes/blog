-module(index_handler).
-behavior(cowboy_handler).

-export([init/2]).

% Responds to "/"
init(Req, State) ->
    {ok, Html} = dtl:render("index.html", #{
        title => <<"Kyle Mayes - Blog">>,
        posts => [ maps:to_list(P) || P <- database:get_posts() ]
    }),
    {ok, cowboy_req:reply(200, #{}, Html, Req), State}.
