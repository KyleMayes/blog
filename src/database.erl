-module(database).

-export([init/0]).
-export([create_post/5]).
-export([get_posts/0]).
-export([get_post/1]).
-export([create_comment/4]).
-export([get_comments/1]).

populate_post(Title, Date, Preview, Contents, Comments) ->
    {Long, Short} = Title,
    Permalink = <<"/posts/", Date/binary, "-", Short/binary>>,
    create_post(Long, Date, Preview, Contents, Permalink),
    Post = get_post(Permalink),
    lists:foreach(
        fun({CName, CDate, CContents}) ->
            create_comment(Post, CName, CDate, CContents)
        end,
        Comments
    ).

% Populate the database with test data
populate() ->
    populate_post(
        {<<"One Simple Trick to Pass CS 7125">>, <<"three-simple-ways">>},
        <<"2017-04-30">>,
        <<"You won't believe the signals your eyes are sending to your brain!">>,
        <<"1. Write an awesome blog for your presentation.">>,
        [{<<"Kyle Mayes">>, <<"2017-04-30">>, <<"Thanks. I followed this advice and passed CS 7125. Now I work at Amazon and make $NaN a year.">>}]
    ),
    populate_post(
        {<<"Stay-at-home Student Discovers Way to Pass Class">>, <<"professors-hate-him">>},
        <<"2017-04-29">>,
        <<"Professors hate him!">>,
        <<"Write clickbait blog posts on your awesome blog that you made for your presentation.">>,
        [{<<"Dr. Professor">>, <<"2017-04-30">>, <<"I laughed, I cried. A+! 100!">>}]
    ).

% Initialize the database
init() ->
    % Open the database
    {ok, _} = sqlite3:open(database, [in_memory]),

    % Create the post table
    sqlite3:create_table(database, post, [
        {id, integer, [{primary_key, [asc, autoincrement]}]},
        {title, text, [not_null]},
        {date, text, [not_null]},
        {preview, text, [not_null]},
        {contents, text, [not_null]},
        {permalink, text, [not_null]}
    ]),

    % Create the comment table
    sqlite3:create_table(database, comment, [
        {id, integer, [{primary_key, [asc, autoincrement]}]},
        {post_id, integer, [not_null]},
        {name, text, [not_null]},
        {date, text, [not_null]},
        {contents, text, [not_null]}
    ]),

    populate().

% Converts the supplied tuple into a list
to_list(Tuple) -> to_list(Tuple, size(Tuple), []).
to_list(_, 0, List) -> List;
to_list(Tuple, N, List) -> to_list(Tuple, N - 1, [element(N, Tuple) | List]).

% Combines the columns and rows into maps
map_rows(Columns, Rows) ->
    Atoms = [ list_to_atom(C) || C <- Columns ],
    [ maps:from_list(lists:zip(Atoms, to_list(R))) || R <- Rows ].

% Returns the rows returned by executing the supplied query
get_rows(Result) ->
    case Result of
        [{columns, Columns}, {rows, Rows}] -> map_rows(Columns, Rows);
        {error, Code, Reason} -> {error, Code, Reason}
    end.

% Inserts a new blog post into the database
create_post(Title, Date, Preview, Contents, Permalink) ->
    Query = "INSERT INTO post (title, date, preview, contents, permalink) VALUES (?, ?, ?, ?, ?)",
    {_, _} = sqlite3:sql_exec(database, Query, [Title, Date, Preview, Contents, Permalink]).

% Returns the posts stored in the database
get_posts() ->
    get_rows(sqlite3:sql_exec(database, "SELECT * FROM post")).

% Returns the post with the supplied permalink
get_post(Permalink) ->
    hd(get_rows(sqlite3:sql_exec(database, "SELECT * FROM post WHERE permalink = ?", [Permalink]))).

% Inserts a new comment into the database
create_comment(Post, Name, Date, Contents) ->
    Query = "INSERT INTO comment (post_id, name, date, contents) VALUES (?, ?, ?, ?)",
    {_, _} = sqlite3:sql_exec(database, Query, [maps:get(id, Post), Name, Date, Contents]).

% Returns the comments stored in the database for the supplied post
get_comments(Post) ->
    Query = "SELECT * FROM comment WHERE post_id = ?",
    get_rows(sqlite3:sql_exec(database, Query, [maps:get(id, Post)])).
