-module(health_handler).
-behavior(cowboy_handler).

-export([init/2]).

% Responds to "/_ah/health"
init(Req, State) ->
    {ok, cowboy_req:reply(200, #{<<"content-type">> => <<"text/plain">>}, <<"OK">>, Req), State}.
