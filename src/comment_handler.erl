-module(comment_handler).
-behavior(cowboy_handler).

-export([init/2]).

% Responds to "/comment"
init(Req, State) ->
    Parameters = maps:from_list(cowboy_req:parse_qs(Req)),
    Permalink = maps:get(<<"permalink">>, Parameters),
    Name = maps:get(<<"name">>, Parameters),
    Contents = maps:get(<<"contents">>, Parameters),

    % Add the comment
    Post = database:get_post(Permalink),
    {{Year, Month, Day}, {Hour, Minute, _}} = erlang:localtime(),
    Date = list_to_binary(io_lib:format("~B-~B-~B ~B:~B", [Year, Month, Day, Hour, Minute])),
    database:create_comment(Post, Name, Date, Contents),

    {ok, cowboy_req:reply(302, #{<<"Location">> => Permalink}, <<>>, Req), State}.
