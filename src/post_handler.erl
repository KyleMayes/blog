-module(post_handler).
-behavior(cowboy_handler).

-export([init/2]).

% Responds to "/posts/[...]"
init(Req, State) ->
    Post = database:get_post(maps:get(path, Req)),
    Comments = [ maps:to_list(C) || C <- database:get_comments(Post) ],
    {ok, Html} = dtl:render("post.html", [
        {title, <<"Kyle Mayes - Blog">>},
        {post, maps:to_list(Post)},
        {comments, Comments}
    ]),
    {ok, cowboy_req:reply(200, #{}, Html, Req), State}.
