-module(blog_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

% Starts the application
start(_, _) ->
    % Initialize the database
    database:init(),

    % Define the route handlers
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/static/[...]", cowboy_static, {priv_dir, blog, "static"}},
            {"/", index_handler, []},
            {"/posts/[...]", post_handler, []},
            {"/comment", comment_handler, []},
            {"/_ah/health", health_handler, []}
        ]}
    ]),

    % Start the HTTP server
    {ok, _} = cowboy:start_clear(http, 100, [{port, 8080}], #{env => #{dispatch => Dispatch}}),

    % Connect to the supervisor
    blog_sup:start_link().

% Stops the application
stop(_) ->
    ok.
