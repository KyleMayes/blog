FROM alpine:3.5

# Install packages
RUN apk add --update \
    curl \
    gcc \
    git \
    make \
    musl-dev \
    sqlite-dev \
    erlang=19.1.0-r0 \
    erlang-asn1=19.1.0-r0 \
    erlang-crypto=19.1.0-r0 \
    erlang-dev=19.1.0-r0 \
    erlang-eunit=19.1.0-r0 \
    erlang-inets=19.1.0-r0 \
    erlang-public-key=19.1.0-r0 \
    erlang-sasl=19.1.0-r0 \
    erlang-ssl=19.1.0-r0 \
    erlang-syntax-tools=19.1.0-r0

# Add project files
COPY priv blog/priv/
COPY src blog/src/
COPY rebar.config blog/
COPY relx.config blog/
COPY sys.config blog/

# Build the project
RUN cd blog && curl -O https://s3.amazonaws.com/rebar3/rebar3 && chmod +x rebar3
RUN cd blog && ./rebar3 compile
RUN cd blog/_build/default/lib/sqlite3 && make
RUN cd blog && ./rebar3 as prod release -o release

# Execute the server application
CMD ["./blog/release/blog/bin/blog", "foreground"]
